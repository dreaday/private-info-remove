package com.ksure;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class IpRemove {

	private void replace(StringBuffer buff, String toReplace, String replaceTo) {
		int start;
		while ((start = buff.indexOf(toReplace)) >= 0)
			buff.replace(start, start + toReplace.length(), replaceTo);
	}

	public static void main(String[] args) {

		File[] fileList = FileUtils.getFile(args).listFiles();

		for (File file : fileList) {
			// 디렉토리 인경우 SKIP
			if (file.isDirectory()) {
				continue;
			}


			// 본래의 폴더 경로에 .PROTECTION을 붙인 폴더를 생성한다.
			File parentPath = new File(file.getParentFile().getAbsolutePath() + ".PROTECTION");

			File newFile = null;
			try {
				// .PROTECTION 가 존재하지 않으면 생성한다.
				if (!parentPath.exists()) {
					FileUtils.forceMkdir(parentPath);
				}

				// .PROTECTION 폴더 하위에 수정된 파일을 저장한다.
				newFile = new File(parentPath.getAbsolutePath() + File.separator + file.getName());

				// .PROTECTION 폴더 하위에 이전 파일을 삭제한다.
				FileDeleteStrategy.FORCE.delete(newFile);

				System.out.println("START : " + file.getName());

				// 파일 용량이 큰 경우 OOM 발생하여 장애를 초래하므로 버퍼크기를 키워 Line별로 처리한다.
				BufferedReader reader = IOUtils.toBufferedReader(new InputStreamReader(new FileInputStream(file)), 4096);

				String log = null;

				// 병렬 처리를 위해 Queue를 활용한다.
				// LinkedBlockingQueue는 다중 스레드 접근에 Safe 하다.
				LinkedBlockingQueue<String> linkedBlockingQueue = new LinkedBlockingQueue<String>();

				List<WriteThread> writeThreadList = new LinkedList<>();

				// 동시작업할 Thread 수는 25개.
				for (int i = 0; i < 25; i++) {
					WriteThread writeThread = new WriteThread(linkedBlockingQueue, newFile);
					writeThread.start();
					writeThreadList.add(writeThread);
				}

				// Queue에 데이터를 적재한다.
				while ((log = reader.readLine()) != null) {
					// IP 정규식 필터링
					log = log.replaceAll("(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])"
							, "***IP PROTECTION***");

					// 디렉토리 형식 필터링
					log = log.replaceAll("((?<=/)|((?<=^)(?=\\\\w)))(?!(\\\\w|\\\\\\\\\\\\.)+/.*).*"
							, "***DIRECTORY PROTECTION***");

					// 이메일 형식 필터링
					log = log.replaceAll("(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
							, "***EMAIL PROTECTION***");
					linkedBlockingQueue.put(log);
				}

				// Queue에 적재된 데이터가 없으면 종료.
				while (true) {
					Thread.sleep(5000);
					if (linkedBlockingQueue.isEmpty()) {
						writeThreadList.parallelStream().forEach(writeThread -> writeThread.interrupt());
						break;
					}
				}
			} catch (IOException | InterruptedException e) {
				System.out.println(file.getName() + " : ERROR");
				e.printStackTrace();
			}
		}
	}
}

class WriteThread extends Thread {
	private LinkedBlockingQueue<String> queue;

	private File newFile;

	public WriteThread(LinkedBlockingQueue<String> queue, File newFile) {
		this.queue = queue;
		this.newFile = newFile;
	}

	@Override
	public void run() {
		while (!this.isInterrupted()) {
			String log = null;
			try {
				log = queue.take();
				FileUtils.writeStringToFile(newFile, log + "\n", "UTF-8", true);
			} catch (IOException | InterruptedException e) {
				System.out.println(this.getName() + " DEAD ");
				e.printStackTrace();
				break;
			}
		}
	}
}
